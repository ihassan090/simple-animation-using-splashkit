/*
This program is written by Mr. Ismail Hassan, Student no. BSCP|CS|41|074 of CICRA Campus.
*/

#include "splashkit.h"



void frog_sit(double bg_count)
{
    play_sound_effect("frog");

    draw_bitmap("background", bg_count, -560);
    draw_bitmap("frog_sit_1", 310, 400);
    refresh_screen();
    delay(350);

    draw_bitmap("background", bg_count, -560);
    draw_bitmap("frog_sit_2", 310, 400);
    refresh_screen();
    delay(350);

    draw_bitmap("background", bg_count, -560);
    draw_bitmap("frog_sit_3", 310, 400);
    refresh_screen();
    delay(350);
}

void entance_scene(double bg_count)
{
    play_sound_effect("frog");

    draw_bitmap("background", bg_count, -560);
    draw_text("HAPPY FROG", COLOR_DARK_GREEN, "animation_font", 50, 140, 110);
    draw_text("By Ismail Hassan", COLOR_BLACK, "info_font", 24, 300, 220);
    draw_bitmap("frog_sit_1", 310, 400);
    refresh_screen();
    delay(350);

    draw_bitmap("background", bg_count, -560);
    draw_text("HAPPY FROG", COLOR_DARK_GREEN, "animation_font", 50, 140, 110);
    draw_text("By Ismail Hassan", COLOR_BLACK, "info_font", 24, 300, 220);
    draw_bitmap("frog_sit_2", 310, 400);
    refresh_screen();
    delay(350);

    draw_bitmap("background", bg_count, -560);
    draw_text("HAPPY FROG", COLOR_DARK_GREEN, "animation_font", 50, 140, 110);
    draw_text("By Ismail Hassan", COLOR_BLACK, "info_font", 24, 300, 220);
    draw_bitmap("frog_sit_3", 310, 400);
    refresh_screen();
    delay(350);
}

void end_scene(double bg_count)
{
    play_sound_effect("frog");
    draw_bitmap("background", bg_count, -560);
    draw_text("THE END", COLOR_DARK_GREEN, "animation_font", 50, 240, 110);
    draw_text("By Ismail Hassan", COLOR_BLACK, "info_font", 24, 300, 220);
    draw_bitmap("frog_sit_1", 310, 400);
    refresh_screen();
    delay(350);

    draw_bitmap("background", bg_count, -560);
    draw_text("THE END", COLOR_DARK_GREEN, "animation_font", 50, 240, 110);
    draw_text("By Ismail Hassan", COLOR_BLACK, "info_font", 24, 300, 220);
    draw_bitmap("frog_sit_2", 310, 400);
    refresh_screen();
    delay(350);

    draw_bitmap("background", bg_count, -560);
    draw_text("THE END", COLOR_DARK_GREEN, "animation_font", 50, 240, 110);
    draw_text("By Ismail Hassan", COLOR_BLACK, "info_font", 24, 300, 220);
    draw_bitmap("frog_sit_3", 310, 400);
    refresh_screen();
    delay(350);
}
void frog_eat_butterfly(double bg_count)
{
    draw_bitmap("background", bg_count, -560);
    draw_bitmap("frog_eat_1", 310, 400);
    draw_bitmap("btf_1", 510, 300);
    refresh_screen();
    delay(150);

    draw_bitmap("background", bg_count, -560);
    draw_bitmap("frog_eat_2", 310, 400);
    draw_bitmap("btf_2", 510, 300);
    refresh_screen();
    delay(150);

    draw_bitmap("background", bg_count, -560);
    draw_bitmap("frog_eat_3", 310, 400);
    refresh_screen();
    delay(150);

    draw_bitmap("background", bg_count, -560);
    draw_bitmap("frog_eat_4", 310, 400);
    refresh_screen();
    delay(150);
}

void draw_frog_jump(double bg_count)
{
    draw_bitmap("background", bg_count, -560);
    draw_bitmap("frog_jump_1", 310, 400);
    draw_bitmap("btf_1", 510, 300);
    refresh_screen();
    delay(150);

    bg_count = bg_count - 35;

    draw_bitmap("background", bg_count, -560);
    draw_bitmap("frog_jump_2", 300, 400);
    draw_bitmap("btf_2", 510, 300);
    refresh_screen();
    delay(150);

    bg_count = bg_count - 35;

    draw_bitmap("background", bg_count, -560);
    draw_bitmap("frog_jump_3", 300, 400);
    draw_bitmap("btf_3", 510, 300);
    refresh_screen();
    delay(150);

    bg_count = bg_count - 35;

    draw_bitmap("background", bg_count, -560);
    draw_bitmap("frog_jump_1", 310, 400);
    draw_bitmap("btf_1", 510, 300);
    refresh_screen();
    delay(150);

}
int main()
{
    //------------------------------ Resource Loading ----------------------------//

    // frog jump sequence
    load_bitmap("frog_jump_1", "frog_jump_01.png");
    load_bitmap("frog_jump_2", "frog_jump_02.png");
    load_bitmap("frog_jump_3", "frog_jump_03.png");

    // frog sitting sequence
    load_bitmap("frog_sit_1", "frog_sit_01.png");
    load_bitmap("frog_sit_2", "frog_sit_02.png");
    load_bitmap("frog_sit_3", "frog_sit_03.png");

    // frog eating butterfly sequence
    load_bitmap("frog_eat_1", "frog_eat_01.png");
    load_bitmap("frog_eat_2", "frog_eat_02.png");
    load_bitmap("frog_eat_3", "frog_eat_03.png");
    load_bitmap("frog_eat_4", "frog_eat_04.png");

    // butterfly flying sequence
    load_bitmap("btf_1", "btf_01.png");
    load_bitmap("btf_2", "btf_02.png");
    load_bitmap("btf_3", "btf_03.png");

    // backgroung
    load_bitmap("background", "back2.jpg");

    // fonts
    load_font("animation_font", "Fluo Gums.ttf");
    load_font("info_font", "ARLRDBD.TTF");

    // backgound nature sound
    load_sound_effect("nature", "nature.wav");

    // frog sound
    load_sound_effect("frog", "frog1.wav");

    // open window
    open_window("Animation by Ismail Hassan", 800, 600);

    double bg_count = 0;
    

    //------------------------------ Entance Scene ----------------------------//
    entance_scene(bg_count);
    entance_scene(bg_count);
    entance_scene(bg_count);
    entance_scene(bg_count);
    entance_scene(bg_count);
    entance_scene(bg_count);


    //------------------------------ Frog Scene ----------------------------//
    // start backgound sound
    play_sound_effect("nature");

    // start frog run after butterfly
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);
    draw_frog_jump(bg_count);

    // frog eat butterfly
    frog_eat_butterfly(bg_count);

    // frog resting
    frog_sit(bg_count);
    frog_sit(bg_count);
    frog_sit(bg_count);


    //------------------------------ Exit Scene ----------------------------//
    end_scene(bg_count);
    end_scene(bg_count);
    end_scene(bg_count);
    end_scene(bg_count);
    end_scene(bg_count);
    end_scene(bg_count);
    end_scene(bg_count);
    end_scene(bg_count);
    end_scene(bg_count);
    end_scene(bg_count);
    

    return 0;
}